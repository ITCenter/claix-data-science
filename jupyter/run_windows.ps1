Function Test-CommandExists{
 Param ($command)
 $oldPreference = $ErrorActionPreference
 $ErrorActionPreference = "stop"
 Try {If(Get-Command $command){Return $true}}
 Catch {Return $false}
 Finally {$ErrorActionPreference=$oldPreference}
}

$port = Get-Random -Max 32767 -Min 10001
Write-Host "Starting JupyterNotebook and HTTP tunnel on port $port ...";

$command = "mkdir -p 'jupyter-notebook' && cd 'jupyter-notebook' && python3 -m venv '.env' && source .env/bin/activate && unset PYTHONPATH && pip install --upgrade pip && pip install --upgrade jupyter && jupyter-notebook --no-browser --port=${port}"

if(Test-CommandExists("ssh")){
    Write-Host "... using existing openSSH";
    $user = Read-Host -Prompt "Username";
    Start-Process ssh -ArgumentList "$user@login18-1.hpc.itc.rwth-aachen.de -L ${port}:localhost:${port} -t `"$command; zsh`"";
}else{
    Write-Host "... using KiTTY SSH";       
    If(!(Test-Path ".\tmp"))
    {
          New-Item -ItemType Directory -Path ".\tmp"
    }
    If(!(Test-Path ".\tmp\kitty.exe"))
    {
        Invoke-WebRequest -Uri "http://www.9bis.net/kitty/files/kitty_portable.exe" -OutFile ".\tmp\kitty.exe";
    }    
    Start-Process .\tmp\kitty -ArgumentList "-ssh login18-1.hpc.itc.rwth-aachen.de -L ${port}:localhost:${port} -cmd `"$command`"" 
}
Write-Host "Exiting.";