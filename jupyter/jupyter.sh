#!/usr/bin/env zsh
 
#SBATCH --nodes 1
#SBATCH --ntasks-per-node 1
#SBATCH --mem-per-cpu 8G
#SBATCH --time 0-1:00:00
#SBATCH --job-name jupyter-notebook
#SBATCH --gres=gpu:1
#SBATCH --output logs/jupyter-notebook-%J.log

if [ -n "${SLURM_JOB_NAME}" ] 
then
    
  # get tunneling info
  XDG_RUNTIME_DIR=""
  port=$(shuf -i8000-9999 -n1)
  node=$(hostname -s)
 
  # Load the latest python
  module switch intel gcc
  module load python/3.6.0
  module load cuda/90
  module load cudnn/7.0.5

  python3 -m venv ".env"
  source .env/bin/activate

  unset PYTHONPATH
 
  #Install jupyter
  pip install jupyter
 
  jupyter-notebook --no-browser --port=${port} --ip=${node}

else

  echo "Canceling previous jobs"

  # cancel previously running notebook jobs
  scancel -n "jupyter-notebook" -u$(whoami)

  echo "Submitting job ${0} to SLURM"

  # schedule the job
  JOBID=$(sbatch "${0}" | grep -io "[0-9]*")
  echo "SLURM job id: ${JOBID}"

  # cleanup logs
  rm logs/*.log  
  mkdir -p logs
  touch ./logs/jupyter-notebook-${JOBID}.log

  # wait for the log file to contain the url
  ( tail -f ./logs/jupyter-notebook-${JOBID}.log & ) | grep -q "http://.*\\?token=.*"
  JUPYTERURL=$(grep -h -i -o "http://.*\\?token=.*" ./logs/jupyter-notebook-${JOBID}.log | head -1)

  echo "Jupyter server running on:"
  echo $JUPYTERURL

fi
