# Running Jupyter Notebook on CLAIX

A collection of snippets and ideas to run data science applications on CLAIX.

## Running Jupyter Notebook on CLAIX

### tl;dr

Run `zsh jupyter.sh`

### Details

Look at the SLURM settings in `jupyter.sh`. Adjust according to your needs, e.g. add CPU, GPU oder RAM requirements. 

Scheduling a job that needs GPU may take significantly longer.

Run `zsh jupyter.sh`

After the job was scheduled a URL is displayed. Set up an SSH tunnel to the host and port shown using local port 8800.

Copy the token. Then open your local browser and go to http://localhost:8800/ enter the token. 

Enjoy.

## Running Tensorflow With GPU on CLAIX

Make sure Jupyter is running on a node with GPU.

`jupyter.sh` will load CUDA v9.0 and CUDNN v7 compatible with tensorflow 1.12. The CLAIX module system currently does not meet the requiements of Tensorflow 1.13 (CUDA 10, CUDNN 7)

In jupyter terminal install tensorflow
```
pip install tensorflow-gpu==1.12.2
```

Restart Jupyter kernel if already running.

Running 
```
from tensorflow.python.client import device_lib
print(device_lib.list_local_devices())
```
within a notebook should display GPUs available. 

Enjoy.
