#!/usr/bin/env zsh

module load python/3.7.3
[ -d .venv ] || python3 -m venv ./.venv
source ./.venv/bin/activate
unset PYTHONPATH
module load python/3.7.3
pip install -r requirements.txt

echo "you can now run"
echo "> source ./.venv/bin/activate"
echo "> unset PYTHONPATH"
echo "> python3 main.py"