# S3 Sample Script

## How To

Instructions for running on CLAIX. All steps are also available in `init.sh`.

### Load Python 3

```sh
module load python/3.7.3
```

### Initialize Virtual Environment

```sh
python3 -m venv ./.venv
source ./.venv/bin/activate
unset PYTHONPATH
```

### Install requirements 

Use

```sh
pip install --upgrade pip
pip install --upgrade boto3
```

or

```sh
pip install -r requirements.txt
```

### Configure

Add `config.json` (see `config.sample.json` for a sample)

### Run

```sh
python3 main.py
```