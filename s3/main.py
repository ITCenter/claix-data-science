# Copyright 2013. Amazon Web Services, Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Source code based on original from
# aws-python-sample
# Copyright 2013 Amazon.com, Inc. or its affiliates. All Rights Reserved. 
# https://github.com/aws-samples/aws-python-sample/blob/master/s3_sample.py
# under the Apache License, Version 2.0

# Modified: 
# * Settings from configuration file
# * Full cURL commands displayed for pre-signed URLs
# * Added pre-signes PUT-Object URL
# * Added code to upload files from a folder to a specific prefix

import boto3
from botocore.client import Config
import os
import json

config = {}
with open('./config.json') as json_file:
    config = json.load(json_file)

# Basic configuration of the low level S3 client

s3client = boto3.client('s3', 'rwth', 
    config=Config(s3={'addressing_style': 'path'}), 
    aws_access_key_id=config['access_key'],
    aws_secret_access_key=config['secret_key'],
    endpoint_url=config['endpoint']
)
bucket_name = config['bucket']

# Very basic operation upload an object with "string" content

object_key = 'python_sample_key.txt'
print('Uploading some data to {} with key: {}'.format(
    bucket_name, object_key))
s3client.put_object(Bucket=bucket_name, Key=object_key, Body=b'Hello World!')

# Using the client, you can generate a pre-signed URL that you can give
# others to securely share the object without making it publicly accessible.
# By default, the generated URL will expire and no longer function after one
# hour. You can change the expiration to be from 1 second to 604800 seconds
# (1 week).

url = s3client.generate_presigned_url(
    'get_object', {'Bucket': bucket_name, 'Key': object_key})
print('\nTry this cURL to download the object:')
print('curl "' + url + '"')

try:
    input = raw_input
except NameError:
    pass
input("\nPress enter to continue...")

# The same works for pre-signing an upload URL that you can give to others to
# put an object into the bucket. This can then be done with cURL respectively.
# The sample provided uses a static string but it can be changed to read a 
# binary file easily.

url = s3client.generate_presigned_url(
    'put_object', {'Bucket': bucket_name, 'Key': object_key})
print('\nTry this cURL to download the object:')
print('curl -X PUT -H "Content-Type:text/xml" -d "Hello from cURL!" "' + url + '"')

try:
    input = raw_input
except NameError:
    pass
input("\nPress enter to continue...")

# Now that you got the hang of the Client API, let's take a look at Resouce
# API, which provides resource objects that further abstract out the over-the-
# network API calls.
# Here, we'll instantiate and use 'bucket' or 'object' objects.

print('\nNow using Resource API')
# First, create the service resource object the same way it was done for the low
# level api.
s3resource = boto3.resource('s3', 
    config=Config(s3={'addressing_style': 'path'}), 
    aws_access_key_id=config['access_key'],
    aws_secret_access_key=config['secret_key'],
    endpoint_url=config['endpoint']
)
# Now, the bucket object
bucket = s3resource.Bucket(bucket_name)
# Then, the object object
obj = bucket.Object(object_key)
print('Bucket name: {}'.format(bucket.name))
print('Object key: {}'.format(obj.key))
print('Object content length: {}'.format(obj.content_length))
print('Object body: {}'.format(obj.get()['Body'].read()))
print('Object last modified: {}'.format(obj.last_modified))

# Lets put some local files in a "folder-like" structure. S3 buckets offer
# prefixes for this.
sample_prefix = 'sample/'

# We are looking for some sample files on the local file system.
for root,dirs,files in os.walk('../data/'):
    for file in files:
        f = os.path.join(root,file)
        s = os.path.getsize(f)
        
        def printBytes(x):
            print('{} {}/{} bytes'.format(file, x, s))

# And now upload them to the bucket. Boto will automatically use some multi
# threading to speed up the upload.
        bucket.upload_file(
            f, 
            sample_prefix + file, 
            Callback=printBytes
        )

for obj in bucket.objects.filter(Prefix=sample_prefix):
    print('{} {} {} bytes'.format(obj.key, obj.last_modified, obj.size))

# Let's keep using the Resource API to delete everything. Here, we'll utilize 
# the collection 'objects' and its batch action 'delete'. Batch actions return a
# list of responses, because boto3 may have to take multiple actions iteratively
# to complete the action.

print('\nDeleting all objects with sample prefix {}/{}.'.format(bucket_name, sample_prefix))
delete_responses = bucket.objects.filter(Prefix=sample_prefix).delete()
for delete_response in delete_responses:
    for deleted in delete_response['Deleted']:
        print('\t Deleted: {}'.format(deleted['Key']))

