# Accessing some Data Science Technologies on CLAIX

See subfolders for detailed descriptions

## [Jupyter](./jupyter/README.md)

Sample Shell scripts to run Jupyter Notebooks as a schedules Job on CLAIX.

## [S3](./s3/README.md)

Sample python scripts to interact with s3 object storage using to boto3 library
on CLAIX.

# License

Unless otherwise noted the source code is licensed under the MIT license.